import React from 'react';
import '../styles/list.css';
import TodoListActions from "./TodoListActions";
import Tooltip from "@material-ui/core/Tooltip";
import Zoom from "@material-ui/core/Zoom";
import {connect} from "react-redux";

class TodoList extends React.Component {
    render() {
        const iList= this.props;
        return (
            <ol>
                {iList.todos.filter(item => item.tag === iList.filter || iList.filter === "All").map(item => (

                    <li
                        key={item.id}
                        onClick={() => {
                            this.handleSoftDelete(item.id, item.tag === "Todo" ? "Finished" : "Todo");

                        }}
                    >
                        <Tooltip key={item.id} TransitionComponent={Zoom} interactive placement= "left" title={item.tag === "Todo" ? "Click to Finish task" : "Click to Bring back task"}>
                            <span className={item.tag === "Todo" ? "listItem" : "listItemDeleted"}>{item.text}</span>
                        </Tooltip>
                        <TodoListActions id = {item.id}/>
                    </li>

                ))}
            </ol>
        );
    }
    handleSoftDelete = (index, filter) => {
        this.props.dispatch({
            type: "SOF_DELETE_TODO",
            items: this.props.todos.map(x =>{
                if(x.id === index){
                    x.tag = filter;
                }
            }),
        });
    };
}

const mapStateToProps = (state) => {
    return {
        todos: state.todos,
        filter: state.filter
    };
};

export default connect(mapStateToProps)(TodoList);
