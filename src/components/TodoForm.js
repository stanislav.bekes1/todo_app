import React from 'react';
import TextField from "@material-ui/core/TextField";
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux';



class TodoForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: ''};
    }
    render() {
        const SearchButton = () => (
            <IconButton color="primary" onClick={this.onSubmit} style = {{padding: 0}}>
                <AddIcon />
            </IconButton>
        );


        return (
            <form style={{width: "100%"}}>
                <TextField style={{ padding: '3%'}}
                           id="outlined-basic"
                           label="What needs to be done?"
                           variant="outlined"
                           onChange={this.handleChange}
                           value={this.state.text}
                           InputProps={{endAdornment: <SearchButton/>}}
                />
            </form>
        )
    }

    onSubmit = () =>{
        const newItem = {
            text: this.state.text,
            id: Date.now(),
            tag: "Todo"
        };
        this.props.dispatch({
            type: "ADD_TODO",
            item: newItem
        });

        this.setState({ text: '' });
    };
    handleChange = (e) => {
        this.setState({ text: e.target.value });
    };
}
const mapStateToProps = state => ({
    text: state.text
});
export default connect(mapStateToProps)(TodoForm);
