import React from 'react';
import Button from "@material-ui/core/Button";
import '../styles/listActions.css';
import {connect} from "react-redux";


class TodoListActions extends React.Component {
    render() {
        const iList= this.props;
        return (
                <Button color="secondary"
                        className="delete is-pulled-right"
                        onClick={() => {
                            this.onDelete(iList.id)
                        }}>x
                </Button>
        );
    }
    onDelete = (index) =>{
        this.props.dispatch({
            type: "DELETE_TODO",
            items: this.props.todos.filter(item => item.id !== index),
        });
    };
}

const mapStateToProps = (state) => {
    return {
        todos: state.todos,
        filter: state.filter
    };
};
export default connect(mapStateToProps)(TodoListActions);
