import React from "react";
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import {connect} from "react-redux";

class TodoFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = { filter: 'All'};
    }
    render() {
        return (
            <ToggleButtonGroup
                value={ this.state.filter}
                exclusive
                onChange={this.handleAlignment}
                aria-label="text alignment"
            >
                <ToggleButton value="All" aria-label="left aligned">
                    All
                </ToggleButton>
                <ToggleButton value="Todo" aria-label="centered">
                    To be done
                </ToggleButton>
                <ToggleButton value="Finished" aria-label="right aligned">
                    Finished
                </ToggleButton>
            </ToggleButtonGroup>

        );
    }

    handleAlignment = (event, newAlignment) => {
        this.props.dispatch({
            type: "CHANGE_FILTER",
            filter: newAlignment
        });
        this.setState({ filter: newAlignment});

    };

}

const mapStateToProps = state => {
    return { filter: state.filter };
};


export default connect(mapStateToProps)(TodoFilter);
