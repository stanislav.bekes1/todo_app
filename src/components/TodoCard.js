import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import TodoList from "./TodoList";
import TodoForm from "./TodoForm";
import TodoFilter from "./TodoFilter";
import '../styles/card.css';

class TodoCard extends React.Component {
    render() {
        const classes = this.props;

        return (
            <Card >
                <CardHeader
                    title="Todo"
                    subheader="Made with raccoon's love"
                />
                <TodoFilter/>
                <CardContent >
                    <TodoList />
                </CardContent>
                <CardActions disableSpacing >
                    <TodoForm  />
                </CardActions>
            </Card>
        );
    }
}

export default (TodoCard);
