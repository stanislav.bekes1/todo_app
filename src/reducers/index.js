 const rootReducer = (state = {todos: [], text: '', filter: "All"}, action) => {
    switch (action.type) {
        case "ADD_TODO":
            return {...state, todos: [...state.todos, action.item]};

        case "DELETE_TODO":
            return {...state, todos: action.items};

        case "SOFT_DELETE_TODO":
            return {...state, todos: action.items};

        case "CHANGE_FILTER":
            return {...state, filter : action.filter};

        default:
            return state;
    }
};

 export default rootReducer;
