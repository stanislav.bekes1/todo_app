import React from 'react';
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import TodoCard from "./components/TodoCard";

class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: [], text: '', filter: 'All' };
  }

  render = () => {
    return (
      <div>
        <Container  align = "center" style={{ backgroundColor: '#131313',maxWidth: "100%" }}>
          <Grid container item xs={6} alignItems="center" direction="column" justify="center" style={{height: '100vh' }}>
            <TodoCard />
            <Grid container item  justify="center" style={{ backgroundColor: '#fcaf82'}}>

            </Grid>
          </Grid>
      </Container>
      </div>
    );
  };
}

export default TodoApp;
